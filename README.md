# Example Python bindings for [`columnarprototype`](https://gitlab.cern.ch/krumnack/columnarprototype)

## Set up

### CVMFS

To build on any CVMFS enabled machine

```
bash build_local.sh
```

which is doing something similar to

```
asetup AnalysisBase,24.2.3
cmake -S src -B build
cmake --build build --clean-first
PYTHONPATH="$(dirname $(find . -type f -iname "PyColumnarPrototype*.so")):${PYTHONPATH}" python3 -c 'import PyColumnarPrototype; print(f"{PyColumnarPrototype.column_maker()=}")'
```

### Docker container

To get a local development environment inside of an AnalysisBase docker image run

```
bash run_docker_env.sh
```

which will drop you into an interactive session with your current working directory bindmounted to `/workdir/` in the container.
`build_local.sh` will work here too.
