import time
from pathlib import Path

import awkward as ak
import numpy as np
import PyColumnarPrototype
import uproot


class MuonTool:
    def __init__(self):
        self._tool = PyColumnarPrototype.ToolHolder()

    def compute(self, muons):
        pt = np.asarray(muons.pt.layout.content, np.float32)
        eta = np.asarray(muons.eta.layout.content, np.float32)
        offsets = np.asarray(muons.pt.layout.offsets, np.uint)

        outputs = np.empty(pt.shape, np.float32)

        self._tool.setColumns("Muons_eta", eta)
        self._tool.setColumns("Muons_pt", pt)
        self._tool.setColumns("Muons", offsets)

        self._tool.setColumns("Muons_output", outputs)
        self._tool.compute(offsets[0], offsets[-1])

        return ak.Array(
            ak.contents.ListOffsetArray(
                offsets=ak.index.Index64(offsets),
                content=ak.contents.NumpyArray(outputs),
            )
        )


# TODO: Make configurable from CLI
pool_file = "DAOD_PHYSLITE.32056469._000452.pool.root.1"
local_path = Path(__file__).parent / "data" / "mc20_13TeV"
if local_path.exists():
    input_file_path = local_path / pool_file
elif Path("/afs/cern.ch/user/e/ekourlit/public").exists():
    input_file_path = "/afs/cern.ch/user/e/ekourlit/public/mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_PHYSLITE.e6337_s3681_r13167_p5511/mc20_13TeV"
else:
    raise SystemExit(f"Please specify a valid input path for {pool_file}")

muoncptool = MuonTool()
fl = uproot.iterate(
    f"{input_file_path}:CollectionTree", filter_name="/AnalysisMuonsAuxDyn.*/"
)

events = next(fl)
muons = ak.zip(
    {"pt": events["AnalysisMuonsAuxDyn.pt"], "eta": events["AnalysisMuonsAuxDyn.eta"]}
)


def test_atlas_cpp():
    """Function to be used by timeit.timeit for ATLAS C++"""
    return muoncptool.compute(muons)


def test_numpy_op():
    """Function to be used by timeit.timeit for NumPy reference"""
    return muons.pt * np.cosh(muons.eta)


if __name__ == "__main__":
    print(f"muons.type: {muons.type}")

    start = time.time()
    result = muoncptool.compute(muons)
    atlas_cpp_time = time.time() - start

    start = time.time()
    reference = muons.pt * np.cosh(muons.eta)
    numpy_time = time.time() - start

    result_title = "Result"
    print(f"\n# {result_title: <11}: {result}")
    print(f"# Expectation: {reference}")
    print(f"# ATLAS C++ delta: {atlas_cpp_time:.5f}")
    print(f"# NumPy operation delta: {numpy_time:.5f}")
    print(f"# ATLASCpp/python: {atlas_cpp_time/numpy_time:.5f}")
